/*
Copyright (c) Microsoft Open Technologies, Inc.  All Rights Reserved.
Licensed under the Apache License, Version 2.0.  See License.txt in the project root for license information.
*/

/*jshint node: true*/

module.exports = function(ctx) {

  var path = require("path"),
    fs = require("fs"),
    fsExtra = require('fs-extra'),
    shell = require("shelljs"),
    rootdir = process.argv[2],
    platform = rootdir + "/platforms/",
    configJSONPath = "./www/props/config.json",
    appInsightsPath = "platforms/windows/www/plugins/cordova-plugin-ms-appinsights/www/AppInsights.js";

  try {
    var configJSON = fsExtra.readJsonSync(configJSONPath, {
      throws: false
    });
    var env = configJSON.global.environment;
    var instrumentation_key = configJSON["env:" + env].app_insights_instrumentation_key;
    var appInsightJS = fsExtra.readFileSync(appInsightsPath, 'utf8');
    appInsightJS = appInsightJS.replace(/instrumentationKey*.*/g, 'instrumentationKey: "$INSTRUMENTATION_KEY",');
    appInsightJS = appInsightJS.replace("$INSTRUMENTATION_KEY", instrumentation_key);

    fsExtra.writeFileSync(appInsightsPath, appInsightJS);
    console.log("> Updated the AppInsights $INSTRUMENTATION_KEY");
  } catch (e) {
    console.log("AppInsights windows $INSTRUMENTATION_KEY update failed");
  }
};
